from django.shortcuts import render, reverse
import requests
import json

def index(request):
    name_from_table = request.GET.get('search')
    connection = 'https://dekontaminasi.com/api/id/covid19/hospitals'
    response = requests.get(connection).json()

    return render(request, 'index.html', {'response': response})