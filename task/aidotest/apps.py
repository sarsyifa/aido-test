from django.apps import AppConfig


class AidotestConfig(AppConfig):
    name = 'aidotest'
