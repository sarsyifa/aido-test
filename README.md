Hello,

I made two version to do this task. The first one I made using django to fetch the given API because I have a trouble when request the HTTP Request using react. I got the CORS error `Access to script at 'https://dekontaminasi.com/api/id/covid19/hospitals' from origin 'http://localhost:3000' has been blocked by CORS policy: The 'Access-Control-Allow-Origin' header has a value 'https://kawalcovid19.id' that is not equal to the supplied origin`. I have tried several ways to fix it, but still it didn't work. So, I made the django app in the `task` folder.

To complete the react task, I was just thinking about fetching the JSON manually and export it locally. So I made the second app using react to create a filter and show the alert. 

Thank you

## Run django-app

1. Install PIP
Open a command prompt and execute easy_install pip. 
This will install pip on your system. This command will work if you have 
successfully installed Setuptools.
Alternatively, go to http://www.pip-installer.org/en/latest/installing.html 
for installing/upgrading instructions.

2. Install Django
execute the following command: `pip install django`

3. Clone the repository to the local machine: `git clone https://gitlab.com/sarsyifa/aido-test.git`

4. Go to `task` folder and execute the webapp using `python manage.py runserver`

## Run react app
1. Clone the repository and go to `react-a` folder
2. Execute using `npm start`