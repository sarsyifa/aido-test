import React, { Fragment, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import dummyRs from './dummy/hospitals.js'
import Autocomplete from '@material-ui/lab/Autocomplete';
import './App.css';
import { AppBar, List, ListItem, ListItemIcon, ListItemText, TextField, Toolbar, Typography } from '@material-ui/core';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

export default function App() {
  const classes = useStyles();
  const [value, setValue] = React.useState(dummyRs[0]['name']);
  const [inputValue, setInputValue] = React.useState("");

  const onChangeHandler = event => {
    setInputValue(event.target.value);
  };
  
  return (
    <div className="App">
        <AppBar color="default" position="static">
        <Toolbar>
          <Typography variant="h5" noWrap>
            Hospitals
          </Typography>

        </Toolbar>
        </AppBar>
        
        <br/>
        <Autocomplete 
            options={dummyRs}
            value={value}
            onChange={(event, newValue) => {
              setValue(newValue);
              if (newValue != null) {
                console.log(newValue.address);
                console.log(newValue.phone);
                console.log(newValue.region);
                console.log(newValue.province);
                alert( newValue.name + '\n\n' + newValue.address + '\n'+ newValue.region + ' - ' + newValue.province + '\n' + newValue.phone);
              }
            }}
            inputValue={inputValue}
            onInputChange={(event, newInputValue) => {
              setInputValue(newInputValue);
            }}
            getOptionLabel={(option) => option['name']}
            style={{ width: 300, marginLeft: 15}}
            renderInput={(filter) => <TextField {...filter} label="Combo box" variant="outlined"/>}
          />

        <List>
          { dummyRs.length > 0 ? (
                    dummyRs.map((rs) => (

                      <ListItem button>
                        <ListItemIcon>
                          <LocalHospitalIcon />
                        </ListItemIcon>
                        <ListItemText primary={rs['name']} 
                            secondary={ 
                              <Fragment>{rs['address']} <br/> 
                                <Typography variant='body2'>{rs['region']} - {rs['province']} <br/> {rs['phone']}</Typography>
                              </Fragment>}>
                        </ListItemText>
                        
                      </ListItem>  

                    ))
                  ) : <Fragment/>}
        </List>
    </div>
  );
}